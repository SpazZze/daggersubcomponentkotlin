package com.frogermcs.recipes.dagger_activities_multibinding

import android.app.Activity
import android.app.Application
import android.content.Context
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityComponentBuilder
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.HasActivitySubcomponentBuilders
import com.frogermcs.recipes.dagger_activities_multibinding.di.app.AppComponent
import com.frogermcs.recipes.dagger_activities_multibinding.di.app.DaggerAppComponent
import javax.inject.Provider

/**
 * @author elena
 * @date 29/12/16
 */

class MyApplication : Application(), HasActivitySubcomponentBuilders {

    lateinit var activityComponentBuilders: Map<Class<out Activity>, Provider<ActivityComponentBuilder<*, *>>>

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.create()
        activityComponentBuilders = appComponent.activityComponentBuilders()
    }

    override fun getActivityComponentBuilder(activityClass: Class<out Activity>): ActivityComponentBuilder<*, *>? {
        return activityComponentBuilders[activityClass]?.get()
    }

    companion object {
        fun get(context: Context): HasActivitySubcomponentBuilders {
            return context.applicationContext as HasActivitySubcomponentBuilders
        }
    }
}