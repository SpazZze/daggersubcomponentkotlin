package com.frogermcs.recipes.dagger_activities_multibinding.di.activity

import com.frogermcs.recipes.dagger_activities_multibinding.main_activity.MainActivity
import com.frogermcs.recipes.dagger_activities_multibinding.main_activity.MainActivityComponent
import com.frogermcs.recipes.dagger_activities_multibinding.second_activity.SecondActivity
import com.frogermcs.recipes.dagger_activities_multibinding.second_activity.SecondActivityComponent
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * @author elena
 * @date 29/12/16
 */

@Module(subcomponents = arrayOf(MainActivityComponent::class, SecondActivityComponent::class))
abstract class ActivityBindingModule {

    @Binds
    @IntoMap
    @ActivityKey(MainActivity::class)
    abstract fun mainActivityComponentBuilder(impl: MainActivityComponent.Builder): ActivityComponentBuilder<*, *>

    @Binds
    @IntoMap
    @ActivityKey(SecondActivity::class)
    abstract fun secondActivityComponentBuilder(impl: SecondActivityComponent.Builder): ActivityComponentBuilder<*, *>
}