package com.frogermcs.recipes.dagger_activities_multibinding.di.activity

import android.app.Activity
import dagger.MembersInjector

/**
 * @author elena
 * @date 29/12/16
 */

interface ActivityComponent<A : Activity> : MembersInjector<A>
