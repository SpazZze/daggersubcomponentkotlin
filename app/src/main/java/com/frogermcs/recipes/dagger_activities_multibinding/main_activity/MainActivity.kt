package com.frogermcs.recipes.dagger_activities_multibinding.main_activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.frogermcs.recipes.dagger_activities_multibinding.BaseActivity
import com.frogermcs.recipes.dagger_activities_multibinding.R
import com.frogermcs.recipes.dagger_activities_multibinding.Utils
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.HasActivitySubcomponentBuilders
import com.frogermcs.recipes.dagger_activities_multibinding.second_activity.SecondActivity
import javax.inject.Inject

/**
 * @author elena
 * @date 29/12/16
 */

class MainActivity : BaseActivity() {

    @Inject
    lateinit var mainActivityPresenter: MainActivityPresenter

    @Inject
    lateinit var utils: Utils

    private var textView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById(R.id.textView) as TextView
        mainActivityPresenter.init()
        Log.e("DEV", "onCreate: " + utils.hardcodedText)
    }

    override fun injectMembers(hasActivitySubcomponentBuilders: HasActivitySubcomponentBuilders) {
        (hasActivitySubcomponentBuilders.getActivityComponentBuilder(MainActivity::class.java) as MainActivityComponent.Builder)
                .activityModule(MainActivityComponent.MainActivityModule(this))
                .build()
                .injectMembers(this)
    }

    fun openSecondScreen(v: View) {
        startActivity(Intent(this, SecondActivity::class.java))
    }

    fun updateText(text: String) {
        textView?.text = text
    }
}
