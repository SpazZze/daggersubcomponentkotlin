package com.frogermcs.recipes.dagger_activities_multibinding.second_activity

import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityScope
import javax.inject.Inject

/**
 * @author elena
 * @date 29/12/16
 */

@ActivityScope
class SecondActivityPresenter
@Inject
constructor(private val activity: SecondActivity)
