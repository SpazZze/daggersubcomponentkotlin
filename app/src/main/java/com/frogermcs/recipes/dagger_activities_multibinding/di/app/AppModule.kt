package com.frogermcs.recipes.dagger_activities_multibinding.di.app

import com.frogermcs.recipes.dagger_activities_multibinding.Utils
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author elena
 * @date 29/12/16
 */


@Module
class AppModule {

    @Provides
    @Singleton
    fun provideUtils(): Utils {
        return Utils()
    }

}