package com.frogermcs.recipes.dagger_activities_multibinding.di.activity

/**
 * @author elena
 * @date 29/12/16
 */

interface ActivityComponentBuilder<M : ActivityModule<*>, C : ActivityComponent<*>> {
    fun activityModule(activityModule: M): ActivityComponentBuilder<M, C>
    fun build(): C
}