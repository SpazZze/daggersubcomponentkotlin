package com.frogermcs.recipes.dagger_activities_multibinding.main_activity

import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityComponent
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityComponentBuilder
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityModule
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityScope
import dagger.Module
import dagger.Subcomponent

/**
 * @author elena
 * @date 29/12/16
 */

@ActivityScope
@Subcomponent(modules = arrayOf(MainActivityComponent.MainActivityModule::class))
interface MainActivityComponent : ActivityComponent<MainActivity> {

    @Subcomponent.Builder
    interface Builder : ActivityComponentBuilder<MainActivityModule, MainActivityComponent>

    @Module
    class MainActivityModule internal constructor(activity: MainActivity) : ActivityModule<MainActivity>(activity)
}
