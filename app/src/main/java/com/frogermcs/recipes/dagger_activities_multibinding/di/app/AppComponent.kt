package com.frogermcs.recipes.dagger_activities_multibinding.di.app

import android.app.Activity
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityBindingModule
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityComponentBuilder
import dagger.Component
import javax.inject.Provider
import javax.inject.Singleton

/**
 * @author elena
 * @date 29/12/16
 */

@Singleton
@Component(modules = arrayOf(AppModule::class, ActivityBindingModule::class))
interface AppComponent {
    fun activityComponentBuilders(): Map<Class<out Activity>, Provider<ActivityComponentBuilder<*, *>>>
}