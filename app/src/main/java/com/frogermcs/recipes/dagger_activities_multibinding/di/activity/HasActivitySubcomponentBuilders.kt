package com.frogermcs.recipes.dagger_activities_multibinding.di.activity

import android.app.Activity

/**
 * @author elena
 * @date 29/12/16
 */

interface HasActivitySubcomponentBuilders {
    fun getActivityComponentBuilder(activityClass: Class<out Activity>): ActivityComponentBuilder<*, *>?
}