package com.frogermcs.recipes.dagger_activities_multibinding.di.activity

import dagger.Module
import dagger.Provides

/**
 * @author elena
 * @date 29/12/16
 */

@Module
abstract class ActivityModule<T>(protected val activity: T) {

    @Provides
    @ActivityScope
    fun provideActivity(): T {
        return activity
    }
}
