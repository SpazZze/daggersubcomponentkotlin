package com.frogermcs.recipes.dagger_activities_multibinding.second_activity

import android.os.Bundle
import com.frogermcs.recipes.dagger_activities_multibinding.BaseActivity
import com.frogermcs.recipes.dagger_activities_multibinding.R
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.HasActivitySubcomponentBuilders
import javax.inject.Inject

/**
 * @author elena
 * @date 29/12/16
 */

class SecondActivity : BaseActivity() {

    @Inject
    lateinit var presenter: SecondActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
    }

    override fun injectMembers(hasActivitySubcomponentBuilders: HasActivitySubcomponentBuilders) {
        (hasActivitySubcomponentBuilders.getActivityComponentBuilder(SecondActivity::class.java) as SecondActivityComponent.Builder)
                .activityModule(SecondActivityComponent.SecondActivityModule(this))
                .build()
                .injectMembers(this)
    }
}
