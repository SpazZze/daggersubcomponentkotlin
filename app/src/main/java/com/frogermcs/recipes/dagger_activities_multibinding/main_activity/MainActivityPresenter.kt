package com.frogermcs.recipes.dagger_activities_multibinding.main_activity

import com.frogermcs.recipes.dagger_activities_multibinding.Utils
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityScope
import javax.inject.Inject

/**
 * @author elena
 * @date 29/12/16
 */

@ActivityScope
class MainActivityPresenter
@Inject
constructor(private val activity: MainActivity, private val utils: Utils) {

    fun init() {
        activity.updateText(utils.hardcodedText)
    }
}
