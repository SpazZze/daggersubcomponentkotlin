package com.frogermcs.recipes.dagger_activities_multibinding.second_activity

import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityComponent
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityComponentBuilder
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityModule
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.ActivityScope
import dagger.Module
import dagger.Subcomponent

/**
 * @author elena
 * @date 29/12/16
 */

@ActivityScope
@Subcomponent(modules = arrayOf(SecondActivityComponent.SecondActivityModule::class))
interface SecondActivityComponent : ActivityComponent<SecondActivity> {

    @Subcomponent.Builder
    interface Builder : ActivityComponentBuilder<SecondActivityModule, SecondActivityComponent>

    @Module
    class SecondActivityModule internal constructor(activity: SecondActivity) : ActivityModule<SecondActivity>(activity)
}