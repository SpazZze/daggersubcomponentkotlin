package com.frogermcs.recipes.dagger_activities_multibinding

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.frogermcs.recipes.dagger_activities_multibinding.di.activity.HasActivitySubcomponentBuilders

/**
 * @author elena
 * @date 29/12/16
 */

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupActivityComponent()
    }

    protected fun setupActivityComponent() {
        injectMembers(MyApplication.get(this))
    }

    protected abstract fun injectMembers(hasActivitySubcomponentBuilders: HasActivitySubcomponentBuilders)
}
